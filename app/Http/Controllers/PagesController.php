<?php

namespace App\Http\Controllers;

class PagesController extends Controller {
    
    public function getIndex() {
        return view('pages/welcome');
    }

    public function getAbout() {
        $firstName = 'Jake';
        $lastName = 'Zerafa';

        $fullName = $firstName . ' ' . $lastName;
        $email = 'jake.zerafa86@gmail.com';
        $data = [];
        $data['email'] = $email;
        $data['fullname'] = $fullName;
        return view('pages/about')->withData($data);
    }

    public function getContact() {
        return view('pages/contact');
    }
}
